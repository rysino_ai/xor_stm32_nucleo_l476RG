# Basic Neural Net for STM32 Nucleo L476RG

Simple neural network example. XOR function trained in TensorFlow and run on STM32.

Network: https://colab.research.google.com/drive/1SlSHzG9zcIEikq10faHsqe_QfOMpiERq?usp=sharing

Connections:
![Niezbędne części](readme_images/port_connections.png)

Bibliography
------------

- https://www.st.com/en/embedded-software/x-cube-ai.html#documentation
- https://www.digikey.com/en/maker/projects/intro-to-tinyml-part-1-training-a-model-for-arduino-in-tensorflow/8f1fc8c0b83d417ab521c48864d2a8ec
- https://forbot.pl/blog/sztuczna-inteligencja-na-stm32-przyklad-uzycia-x-cube-ai-id41588

