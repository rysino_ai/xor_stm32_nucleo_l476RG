/**
  ******************************************************************************
  * @file    xor_data_params.h
  * @author  AST Embedded Analytics Research Platform
  * @date    Sat Nov 19 14:56:51 2022
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  ******************************************************************************
  */

#ifndef XOR_DATA_PARAMS_H
#define XOR_DATA_PARAMS_H
#pragma once

#include "ai_platform.h"

/*
#define AI_XOR_DATA_WEIGHTS_PARAMS \
  (AI_HANDLE_PTR(&ai_xor_data_weights_params[1]))
*/

#define AI_XOR_DATA_CONFIG               (NULL)


#define AI_XOR_DATA_ACTIVATIONS_SIZES \
  { 32, }
#define AI_XOR_DATA_ACTIVATIONS_SIZE     (32)
#define AI_XOR_DATA_ACTIVATIONS_COUNT    (1)
#define AI_XOR_DATA_ACTIVATION_1_SIZE    (32)



#define AI_XOR_DATA_WEIGHTS_SIZES \
  { 148, }
#define AI_XOR_DATA_WEIGHTS_SIZE         (148)
#define AI_XOR_DATA_WEIGHTS_COUNT        (1)
#define AI_XOR_DATA_WEIGHT_1_SIZE        (148)



#define AI_XOR_DATA_ACTIVATIONS_TABLE_GET() \
  (&g_xor_activations_table[1])

extern ai_handle g_xor_activations_table[1 + 2];



#define AI_XOR_DATA_WEIGHTS_TABLE_GET() \
  (&g_xor_weights_table[1])

extern ai_handle g_xor_weights_table[1 + 2];


#endif    /* XOR_DATA_PARAMS_H */
