/**
  ******************************************************************************
  * @file    xor_data_params.c
  * @author  AST Embedded Analytics Research Platform
  * @date    Sat Nov 19 14:56:51 2022
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  ******************************************************************************
  */

#include "xor_data_params.h"


/**  Activations Section  ****************************************************/
ai_handle g_xor_activations_table[1 + 2] = {
  AI_HANDLE_PTR(AI_MAGIC_MARKER),
  AI_HANDLE_PTR(NULL),
  AI_HANDLE_PTR(AI_MAGIC_MARKER),
};




/**  Weights Section  ********************************************************/
AI_ALIGNED(32)
const ai_u64 s_xor_weights_array_u64[19] = {
  0x3ec0e02f3f2223e1U, 0xbcc6f35bbd0f2e0eU, 0xbf603206be927455U, 0xbed4a185bed4a919U,
  0xbec28c173e98f3c5U, 0xbd93acadbdf32f0fU, 0x3e90fce03f8eddb5U, 0x3e9ef9dabefa9fa3U,
  0x3ec5528dbf5c4ee0U, 0xbe0df275be5f9a1cU, 0xbedfce12bf32f4fdU, 0xbe310f4c3e9dbc0bU,
  0x3e432cc5bf965d4cU, 0x3f36f7b63f046aa4U, 0xbec7d6c6becb0ca0U, 0xbeb8a538be810896U,
  0xbe9be11fbf649ef4U, 0xbf4247d5bf00fef1U, 0x3f3a56e6U,
};


ai_handle g_xor_weights_table[1 + 2] = {
  AI_HANDLE_PTR(AI_MAGIC_MARKER),
  AI_HANDLE_PTR(s_xor_weights_array_u64),
  AI_HANDLE_PTR(AI_MAGIC_MARKER),
};

